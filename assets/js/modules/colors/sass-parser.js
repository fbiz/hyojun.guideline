// @grunt -task=comp-js -page=guideline

define([
	'mod/colors/color-group'
], function(colorGroup){
	"use strict";

	var SASSParser,
		instances = [],
		_getVarName = function (p_value) {
			return ((p_value || '').match(/(\$.*?):/) || []).pop();
		},
		_getHSLValue = function (p_value) {
			var val = p_value || '',
				pattern = new RegExp(this.hslFunction + '\\((.*?),(.*?),(.*?),(.*?)\\)');
			val = val.match(pattern) || [];
			if (val.length < 2) {
				return null;
			}
			val = val.slice(1);

			return {
				base: val[0],
				h: Number(val[1]),
				s: Number(val[2]),
				l: Number(val[3])
			};
		},
		_getHEXValue = function (p_value) {
			return ((p_value || '').match(/\#[a-f0-9]{3,6}/i) || []).pop();
		},
		_getColorFromLine = function (p_line) {
			var varName = null,
				hsl = _getHSLValue.bind(this)(p_line),
				hex = _getHEXValue.bind(this)(p_line);

			if (hsl || hex) {
				varName = _getVarName.bind(this)(p_line);
				if (varName) {
					this.colors.add(varName, hsl ? null : hex, hsl);
				}
			}
		};


	SASSParser = function(p_config) {
		this.url = p_config.url;
		this.mainPattern = p_config.mainPattern || 'theme';
		this.hslFunction = p_config.hslFunction || 'gl-hsl-diff';
		this.colors = colorGroup.construct();
		this.evt = $(this);
	};

	SASSParser.prototype = {
		init: function () {
			this.load();
		},
		load: function (p_url) {
			$.ajax({
				url: p_url || this.url,
				success: function (response) {
					$.each(response.split(/\n/), function (i, item) {
						_getColorFromLine.bind(this)(item);
					}.bind(this));
					this.colors.calculateRelatives();
					this.evt.trigger('load-success');
				}.bind(this)
			});
		},
		getMainColors: function () {
			return this.colors.filter(this.mainPattern);
		},
		getExceptionColors: function () {
			return this.colors.filter(this.mainPattern, true);
		}
	};

	return {
		construct: function (p_config) {
			var inst = new SASSParser(p_config);
			return inst;
		},
		plugin: function (p_pluginSelector) {
			var className = p_pluginSelector || '.gl-plugin-sass-parser',
				nodes = $(className);

			nodes.each(function (i, item) {
				var holder = $(item),
					inst = new SASSParser({
						url: holder.data('sp-url'),
						mainPattern: holder.data('sp-main-pattern'),
						hslFunction: holder.data('sp-hsl-function')
					});
				instances.push(inst);
				$(this).data('sass-parser', inst);
			});
			return instances;
		}
	};
});