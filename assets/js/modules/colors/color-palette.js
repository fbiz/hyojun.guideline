// @grunt -task=comp-js -page=guideline

define([
	'mod/colors/color-utils'
], function(colorUtils){
	"use strict";

	var colorJs = net.brehaut.Color,
		instances = [],
		ColorPalette = function (p_config) {
			this.holder = p_config.holder;
			this.colorCSSClass = p_config.colorCSSClass;
			this.filterHolder = p_config.filterHolder;
			this.mainPattern = p_config.mainPattern;
			this.colors = [];
			this.keyStatus = 'iddle';
			this.nodes = [];
		};

	ColorPalette.prototype = {
		init: function () {
			this.addToggleHandlers();
			this.addPaletteHandlers();
		},
		createPalette: function () {
			var fragment = document.createDocumentFragment(),
				round = function (v) {
					return Math.round(v * 100) / 100;
				},
				mainPattern = new RegExp('\\$.*?(' + this.mainPattern + ')');

			$.each(this.colors.sort().data, function (i, item) {
				if (!item) {
					return;
				}
				var elm = $(document.createElement('div'))
					.addClass(this.colorCSSClass)
					.css('background', item.hex)
					.attr('data-type', item.label.match(mainPattern) ? 'main' : 'exception')
					.html([
						item.label,
						item.hex,
						'H:'+ round(item.color.getHue()),
						'S:'+ round(item.color.getSaturation()),
						'L:'+ round(item.color.getLightness())
					].join('<br>'));

				elm.click(function(){
					if (this.keyStatus === 'active') {
						elm.hide();
					}
				}.bind(this));

				this.nodes.push(elm);
				fragment.appendChild(elm[0]);
			}.bind(this));

			this.holder[0].appendChild(fragment);
		},
		addPaletteHandlers: function () {
			$(window)
				.keypress(function(e){
					if (e.keyCode === 114) {
						$(this.colorCSSClass, this.holder).show();
					}
				}.bind(this))
				.keydown(function(e){
					if (e.keyCode === 68) {
						this.keyStatus = 'active';
					}
				}.bind(this))
				.keyup(function(e){
					this.keyStatus = 'iddle';
				}.bind(this));
		},
		addToggleHandlers: function () {
			$('input', this.filterHolder).change(function () {
				$('[data-type='+this.value+']').toggle(this.checked);
			});
		},
		status: function (p_status, p_data) {
			switch (p_status) {
				case 'loading':
					this.holder
						.removeClass('loading')
						.addClass('loading');
					break;
				case 'color-ready':
					this.holder.removeClass('loading');
					this.colors = p_data.allColors;
					this.createPalette();
					break;
			}
		}
	};
	
	return {
		construct: function () {
			return new ColorPalette();
		},
		plugin: function (p_pluginSelector) {
			var className = p_pluginSelector || '.gl-plugin-color-palette',
				nodes = $(className);

			nodes.each(function (i, item) {
				var form = $(item),
					inst = new ColorPalette({
						holder: $(form.data('cp-holder'), form),
						filterHolder: $(form.data('cp-filter-holder'), form),
						colorCSSClass: form.data('cp-color-cssclass'),
						mainPattern: form.data('cp-main-pattern') || 'main'
					});
				inst.init();
				instances.push(inst);
				$(this).data('color-generator', inst);
			});
			return instances;
		},
		status: function (p_value) {
			var args = Array.prototype.slice.apply(arguments);
			$.each(instances, function (i, item) {
				item.status.apply(item, args);
			});
		}
	};
});