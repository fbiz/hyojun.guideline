// @grunt -task=comp-js -page=guideline

// needs net.brehaut.Color
define([
	'mod/colors/color-utils'
], function(colorUtils){
	"use strict";

	var colorJs = net.brehaut.Color,
		instances = [],
		ColorGenerator,
		_addHandlers = function () {
			var applyColorResult = function () {
				var newColor = this.colorTo.val().replace(/^([^#])/, '#$1'),
					closest = _getNearestColors.bind(this)(newColor, 'main')[0];
				_applyResult.bind(this)(closest);
				return false;
			}.bind(this);
			
			this.select.change(function (e) {
				var newColor = this.colorTo.val().replace(/^([^#])/, '#$1'),
					closest = this.colors.get(this.select.val());

				_applyResult.bind(this)(closest);

			}.bind(this));
			this.form.submit(applyColorResult);
			this.colorTo.blur(applyColorResult);
		},
		_fillSelect = function () {
			var fragment = document.createDocumentFragment();
			$.each(this.mainColors.data, function (i, item) {
				var option = $(document.createElement('option'));
				option.val(item.label);
				option.text(item.label);
				fragment.appendChild(option[0]);
			});
			this.select[0].appendChild(fragment);
		},
		_updateColorBox = function (p_box, p_color) {
			p_box.each(function (i, item) {
				$(this)
					.css('background', p_color[i].hex)
					.text(p_color[i].label);
			});
		},
		_applyResult = function (base) {
			// TODO: "base" validation
			var colorFrom = base.color,
				colorTo = colorJs(this.colorTo.val().replace(/^([^#])/, '#$1')),
				sassFunction = this.form.data('sass-hsl-function') || 'gl-hsl-diff',
				colorDiff = null;

			colorDiff = colorUtils.getEncodeHSLDiffValues(
				// saturationa and lightness are * 100 to fit sass function (0 to 100)
				[colorFrom.getHue(), colorFrom.getSaturation() * 100, colorFrom.getLightness() * 100],
				[colorTo.getHue(), colorTo.getSaturation() * 100, colorTo.getLightness() * 100]
			);
			this.result.val(sassFunction + '(%s)%s; //%s'
					.replace(/%s/, [base.label].concat(colorDiff).join(', '))
					.replace(/%s/, ' !default')
					.replace(/%s/, colorTo.toCSS())
				);

			_updateColorBox(this.colorBoxFrom, [base]);
			_updateColorBox(this.colorBoxClosest, _getNearestColors.bind(this)(colorTo.toCSS()));
			this.colorBoxTo.css('background', colorTo.toCSS());
		},
		_getNearestColors = function (p_hex, p_filter) {
			var colorList = p_filter === 'main' ? this.mainColors : this.colors;
			return colorList.sortByClosest(p_hex).data;
		};

	ColorGenerator = function(p_config) {
		this.form = p_config.form;
		this.result = p_config.result;
		this.colorTo = p_config.colorTo;
		this.colorBoxFrom = p_config.colorBoxFrom;
		this.colorBoxTo = p_config.colorBoxTo;
		this.colorBoxClosest = p_config.colorBoxClosest;
		this.select = p_config.select;
		this.mainColors = null;
		this.colors = null;
	};
	ColorGenerator.prototype = {
		init: function () {
			_addHandlers.bind(this)();
		},
		status: function (p_status, p_data) {
			switch (p_status) {
				case 'loading':
					this.form
						.removeClass('loading')
						.addClass('loading');
					break;
				case 'color-ready':
					this.form.removeClass('loading');
					this.mainColors = p_data.mainColors;
					this.colors = p_data.allColors;
					_fillSelect.bind(this)();
					break;
			}
		}
	};

	return {
		construct: function () {
			return new ColorGenerator();
		},
		plugin: function (p_pluginSelector) {
			var className = p_pluginSelector || '.gl-plugin-color-generator',
				nodes = $(className);

			nodes.each(function (i, item) {
				var form = $(item),
					inst = new ColorGenerator({
						form: form,
						result: $(form.data('cg-result'), form),
						colorTo: $(form.data('cg-color-to'), form),
						colorBoxFrom: $(form.data('cg-color-box-from'), form),
						colorBoxTo: $(form.data('cg-color-box-to'), form),
						colorBoxClosest: $(form.data('cg-color-box-closest'), form),
						select: $(form.data('cg-select'), form)
					});
				inst.init();
				instances.push(inst);
				$(this).data('color-generator', inst);
			});
			return instances;
		},
		status: function (p_value) {
			var args = Array.prototype.slice.apply(arguments);
			$.each(instances, function (i, item) {
				item.status.apply(item, args);
			});
		}
	};
});