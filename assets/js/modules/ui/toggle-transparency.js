// @grunt -task=comp-js -page=guideline

// needs net.brehaut.Color
define([], function(){
	"use strict";

	var ToggleTransparency,
		instances = [],
		defaultConfig = {
			checkersClass: 'gl-checkers-wrapper'
		};

	ToggleTransparency = function (p_config) {
		this.config = $.extend({}, defaultConfig, p_config);
		this.b = $(document.body);
		this.holder = this.config.holder || $(document.body);
		this.checkersClass = this.config.checkersClass;
	};
	ToggleTransparency.prototype = {
		_buildCheckers: function () {
			// checkers
			this.holder.click(function (e) {
				if (this.b.hasClass(this.checkersClass)) {
					this.b.removeClass(this.checkersClass);
					this._setCookie(this.checkersClass, 'false');
				} else {
					this.b.addClass(this.checkersClass);
					this._setCookie(this.checkersClass, 'true');
				}
				e.preventDefault();
			}.bind(this));
			
			if (this._getCookie(this.checkersClass) === 'true') {
				this.b.addClass(this.checkersClass);
			}
		},
		_setCookie: function (p_label, p_value) {
			document.cookie = [p_label, p_value].join('=');
		},
		_getCookie: function (p_label) {
			var matched = document.cookie.match(new RegExp(p_label+'=(.*?)(?:;|$)'));
			return matched && matched[1];
		},
		init: function () {
			this._buildCheckers();
		}
	};

	return {
		construct: function (p_config) {
			return new ToggleTransparency(p_config);
		},
		plugin: function () {
			$('.gl-plugin-toggle-transparency').each(function (i, item) {
				var inst = new ToggleTransparency({
					holder: $(item)
				});
				inst.init();
				instances.push(inst);
			});
		}
	};
});