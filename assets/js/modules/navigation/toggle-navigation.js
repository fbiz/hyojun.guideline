// @grunt -task=comp-js -page=guideline

// needs net.brehaut.Color
define([], function(){
	"use strict";

	var ToggleNavigation,
		instances = [],
		_isTouch = function() {
			var el = document.createElement('div');
			el.setAttribute('ontouchmove', 'return;');
			return typeof el.ontouchmove === "function";
		},
		_cssPrefixer = function (val) {
			var prop = Modernizr.prefixed(val);
			if (!prop) {
				return "";
			}
			return prop.toLowerCase().toLowerCase().replace(/^(webkit|moz|ms|o)/, "-$1-");
		},
		_transitionEvent = function () {
			var transEndEventName = {
				'WebkitTransition' : 'webkitTransitionEnd',
				'MozTransition'    : 'transitionend',
				'OTransition'      : 'oTransitionEnd',
				'msTransition'     : 'MSTransitionEnd',
				'transition'       : 'transitionend'
			}[ Modernizr.prefixed('transition') ];
			return transEndEventName;
		};

	ToggleNavigation = function (p_config) {
		this.doc = $(document.documentElement);
		this.b = $('body');
		this.holder = p_config.holder;
		this.pageHolder = $(p_config.pageHolder);
		this.menuHolder = $(p_config.menuHolder);
	};
	ToggleNavigation.prototype = {
		_avoidFlick: function () {
			if (navigator.userAgent.match(/iPhone/i)) {
				this.menuHolder.css({
					// '-webkit-transform': 'translateZ(0)',
					'-webkit-backface-visibility': 'hidden'
				});
				this.pageHolder.css({
					// '-webkit-transform': 'translateZ(0)',
					'-webkit-backface-visibility': 'hidden'
				});
			}
		},
		_onToggle: function (e) {
			if (this.doc.hasClass('gl-nav-open')) {
				this.close();
			} else {
				this.open();
			}
			e.preventDefault();
		},
		_addHandlers: function () {
			if (_isTouch()) {
				this.holder
					.on('click', function (e) {
						e.preventDefault();
					})
					.on('touchstart', function (e) {
						e.preventDefault();
					})
					.on('touchend', this._onToggle.bind(this));
			} else {
				this.holder.on('click', this._onToggle.bind(this));
			}
		},
		open: function () {
			var diffWidth = this.holder.offset().left * 2 + this.holder.outerWidth(),
				finalWidth = window.innerWidth - diffWidth;
			this.doc
				.removeClass('gl-nav-open')
				.addClass('gl-nav-open');
			this.b.css({
				'height': this.menuHolder[0].offsetHeight
			});
			this.holder.css(_cssPrefixer('transform'), 'translateX('+finalWidth+'px)');
			this.pageHolder.css(_cssPrefixer('transform'), 'translateX('+finalWidth+'px)');
		},
		close: function () {
			this.holder.one(_transitionEvent(), function () {
				this.doc.removeClass('gl-nav-open');
				this.b.css({
					'height': ''
				});
			}.bind(this));
			this.holder.css(_cssPrefixer('transform'), 'translateX(0)');
			this.pageHolder.css(_cssPrefixer('transform'), 'translateX(0)');
		},
		init: function () {
			this._addHandlers();
			this._avoidFlick();
		}
	};

	return {
		construct: function (p_config) {
			return new ToggleNavigation(p_config);
		},
		plugin: function () {
			$('.gl-plugin-nav-toggle').each(function (i, item) {
				var inst = new ToggleNavigation({
						holder: $(item),
						pageHolder: item.getAttribute('data-nav-toggle-page-holder'),
						menuHolder: item.getAttribute('data-nav-toggle-menu-holder')
					});
				inst.init();
				instances.push(inst);
			});
		}
	};
});