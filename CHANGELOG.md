# Hyojun - SASS Standards Changelog
`v0.1.1`

### 0.1.1
04/04/2014    
@mcarneiro    

* Compatibilidade para IE8;
* Ajustes na versão ASP.NET;
* Remoção dos prefixos `gl-` e `z-index` / `space-size` agora são usados como sufixo;
* Conversão de REM para EM (elementos agora estão flexíveis);

### 0.1.0
04/04/2014    
@mcarneiro    
Primeiro release lançado para equipe
